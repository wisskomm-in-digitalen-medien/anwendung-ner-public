#### 2.7 Wissenschaftliche Disziplin (DISC)

Hier wird das Fachgebiet codiert, auf dem der:die Wissenschaftler:in bzw. die Akteur:in aus der wissenschaftlichen Administration (vorwiegend) tätig ist. Die Einteilung in Fachdisziplinen orientiert sich an der DFG-Fachsystematik (siehe [diese Liste](https://www.dfg.de/download/pdf/dfg_im_profil/gremien/fachkollegien/amtsperiode_2016_2019/fachsystematik_2016-2019_de_grafik.pdf)).     
Codiert werden nur die übergeordneten Fachdisziplinen (Geisteswissenschaften, Sozial- und Verhaltenswissenschaften etc.) – die Unterausprägungen (Ur- und Frühgeschichte, Klassische Philologie etc.) dienen lediglich zur Orientierung, welche einzelnen Wissenschaftsbereiche unter die jeweilige Fachdisziplin fallen.

**1 = Geisteswissenschaften**    
**2 = Sozial- und Verhaltenswissenschaften**    
**3 = Biologie**    
**4 = Medizin**    
**5 = Agrar-, Forstwissenschaften und Tiermedizin**    
**6 = Chemie**    
**7 = Physik**    
**8 = Mathematik**    
**9 = Geowissenschaften**    
**10 = Ingenieurwissenschaften**    
**11 = Akteur:in ist kein:e Wissenschaftlerin (z.B. Sprecher:in)**    
**99 = nicht zu erkennen**

ACHTUNG: In Einzelfällen kann die disziplinäre Verortung schwierig bzw. unklar sein. Bei unseren drei Issues Biotechnologie, Klimawandel und Neurowissenschaften handelt es sich jeweils um sehr interdisziplinärere Forschungszweige. Bitte versuchen Sie, die disziplinäre Angabe so exakt wie möglich aus dem Artikelinhalt zu entnehmen. Kommt beispielsweise in einem Artikel über Neurowissenschaften ein:e „Psycholog:in“ zu Wort, codieren Sie laut DFG-Fachsystematik „2 = Sozial- und Verhaltenswissenschaften“. Wird ein:e „Humangenetiker:in“ erwähnt, codieren Sie laut DFG-Fachsystematik „4 = Medizin“.

Vermutlich werden die disziplinären Bezeichnungen nicht immer so klar erkennbar sein. Versuchen Sie daher aus dem Kontext der Akteursnennung zu erschließen, ob es sich etwa bei eine:r „Biotechnolog:in“ oder „Forscher:in“ beispielsweise etwa um ein:e Biolog:in, Chemik:erin, Ingenieur:in oder Agrarwissenschaflter:in handelt. Geht es in dem Artikel beispielsweise eher um grüne Biotechnologie ist vermutlich „5 = Agrar-, Forstwissenschaften und Tiermedizin“ die zutreffende Disziplin, geht es hingegen um rote Biotechnologie, ist vermutlich „4 = Medizin“ zutreffend usw. Geht es um den Einsatz von Antibiotika in der Tierzucht ist vermutlich „5 = Agrar-, Forstwissenschaften und Tiermedizin“ die zutreffende Disziplin – geht es hingegen um Antibiotikaresistenzen beim Menschen, ist vermutlich „4 = Medizin“ die zutreffende Disziplin.

*Wir legen außerdem fest:*
Wird ein:e Akteur:in unspezifisch als „Klimaforscher:in“ bezeichnet, codieren wir laut DFG-Fachsystematik „9 = Geowissenschaften“. 
„Neurowissenschaflter:innen“ werden laut DFG-Fachsystematik unter „4 = Medizin“ codiert.
„Antibiotikaforschung“ wird, falls nicht näher spezifiziert, unter „4 = Medizin“ codiert.     „Gentechnologie“, „Stammzellenforschung“ oder „Klonen“ wird i.d.R. unter „4 = Medizin“ verortet (es sei denn, es geht speziell um Forschung an Pflanzen und Tieren, ohne dass dies erkennbar als ‚Vorstudie‘ zu humangenetischer Forschung anzusehen ist – dann codieren wir „3 = Biologie“).    
„Meterologie“ wird unter „9 = Geowissenschaften“ codiert.

