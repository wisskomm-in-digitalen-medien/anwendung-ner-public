### I Thema und Ziele des Projekts

Folgende Forschungsfragen möchten wir im Rahmen dieses Projekts beantworten:

Erstens möchten wir herauszufinden, ob sich – abhängig vom Themenfeld/ Issue – Unterschiede ergeben in der Beziehung zwischen der *wissenschaftlichen Reputation* wissenschaftlicher Expert:innen und ihrer *Präsenz in der (medialen) Öffentlichkeit*, das sind die Forschungsfragen (RQs 2a und 2b). Anders formuliert: Wie hoch ist die wissenschaftliche Reputation der in der Medienberichterstattung präsenten wissenschaftlichen Expert:innen und unterscheidet sich diese je nach Themenfeld?

Und wir wollen *zweitens* wissen, ob sich abhängig von der Zeit Veränderungen ergeben (ob etwa die Höhe der Reputation der öffentlich sichtbaren Expert:innen ansteigt), das ist RQ 1.     
Es werden Vergleiche auf zwei Ebenen durchgeführt, 1) ein Vergleich zwischen den Issues und 2) ein Vergleich zwischen zwei Zeiträumen.    
Neben diesen Kernfragen interessieren wir uns darüber hinaus für die Akteursstruktur der vier Issues. Gemeint ist, aus welchen gesellschaftlichen Bereichen (etwa Politik, Wissenschaft, Interessensgruppen) die in der medialen Berichterstattung zu Wort kommenden Akteur:innen stammen . Wir fragen entsprechend:    
RQ 3: Unterscheidet sich die Akteursstruktur der öffentlichen Debatten zwischen den Zeiträumen?    
RQ 4a + b: Unterscheidet sich die Akteursstruktur zwischen den Themenfeldern um die Jahrtausendwende bzw. in 2020?    
Dieses Design ist Teil eines noch um eine weitere, dritte Vergleichsebene erweiterten Studienanlage. Bei der dritten Ebene handelt es sich um einen Vergleich zwischen unterschiedlichen Ländern, in diesem Fall Frankreich. Zeitgleich mit der Projektarbeit ist geplant, mindestens für das Themenfeld der Neurowissenschaften dieselben Fragen auch an die französische Berichterstattung zu stellen. Daraus ergibt sich das folgende Projektdesign.


### II Die Themenfelder/ Issues

Folgende wissenschaftliche Issues analysieren wir hinsichtlich ihrer Akteursstruktur in der Medienberichtersattung:

1.	Biotechnologie
2.	Klimawandel
3.	Neurowissenschaften
4.	Antibiotikaresistenz

### III Die Suchstrings

Anhand folgender Suchstrings erfassen wir die Medienberichterstattung über die vier genannten Issues:

**Biotechnologie**    
((Biotech\*) AND ((gentech\*) OR (genmani\*) OR (genet\*) OR (\*genom\*) OR (synthetisch\* W/1 Biologie) OR (DNA\*) OR (RNA\*) OR (Zellkultur\*) OR (biosens\*) OR (biokataly\*) OR (gentrans\*) OR (\*stammzell\*) OR (molekular\*) OR (Mutation\*) OR (mutier\*) OR (\*klon\*) OR (biomed\*) OR (Genschere) OR (Crispr\*) OR (Gentherapie\*) OR (Zellkern\*) OR (embryo\*) OR (in-vitro\*) OR (ips\*) OR (Keimbahn\*) OR (transgen\*) OR (biorak\*) OR (Stoffwechsel\*) OR (enzym\*) OR (ferment\*) OR (bakteri\*) OR (protein\*) OR (mikrob\*) OR (glucose\*) OR (molekül\*) OR (molekuel\*) OR (kataly\*) OR (Biokraftstoff\*) OR (Biotreibstoff\*) OR (in W/1 vitro) OR (amino\*) OR (biosicherheit\*) OR (GMO\*) OR (GVO\*) OR (DNS\*) OR (Zelltherapie\*) OR (biologisch\* W/1 Sicherheit) OR (rot\* W/1 Gentechnik\*) OR (weiß\* W/1 Gentechnik\*) OR (grün\* W/1 Gentechnik\*)) 

**Klimawandel**    
(((klima\*) AND NOT (Klimaanlage\*) AND NOT (Klimatisier\*) AND NOT (Klimax) AND NOT (Klimatechnik) AND NOT (Betriebsklima) AND NOT (Unternehmensklima)) AND ((Klimawandel\*) OR (Klimakrise\*) OR ((Treibhaus\*) OR (Erderwärmung) OR (Erderwaermung) OR (globale W/1 Erwärmung) OR (globale W/1 Erwaermung) OR (Klimaziel\*) OR (CO2\*) OR (Kohlendioxid\*) OR (Kohlenstoffdioxid\*) OR (Luftverschmutz\*) OR (Umweltverschmutz\*) OR (\*temperatur\*) OR (Methan\*) OR (Klimakatastrophe) OR (Klimatrend) OR (Klimaveränderung) OR (Klimaveraenderung) OR (Klimaänd\*) OR (Klimaaend\*) OR (Klimaforsch\*) OR (Strahlungsantrieb) OR (Klimazustand) OR (Klimawechsel) OR (Klimasystem) OR (Klimaschwank\*) OR (Weltklima\*) OR (Extremwetter\*) OR (Hitzetage) OR (Klimaschutz\*) OR (Erderhitz\*) OR (Klimareport) OR (Klimabilanz) OR (\*klima\*bericht) OR (klimaneutral\*) OR (Klimaminist\*) OR (Klimagipfel) OR (klimaschädlich\*) OR (klimaschaedlich\*) OR (Klimaschütz\*) OR (Klimaschuetz\*) OR (Dürre\*) OR (Duerre\*) OR (Ökosystem\*) OR (Oekosystem\*)  OR (Biomasse\*) OR (umweltpoliti\*) OR (klimapoliti\*) OR (\*emission\*) OR (Stickstoff\*) OR (Meereis\*) OR (Meeresspiegel\*) OR (Klimagas\*) OR (Klimamodell\*) OR (Klimapaket\*) OR (Wetterextrem\*) OR (Klimaverschiebung\*))) 

**Neurowissenschaften**    
(((neuro\*) AND NOT (neurotisch\*) AND NOT (pflege\*) AND NOT (gesundheit\*)) AND ((Hirn\*) OR (kognit\*) OR (Magnetresonanz\*) OR (Kernspint\*) OR (schizophren\*) OR (zerebral\*) OR (cortex) OR (biomarker\*) OR (\*tomogra\*) OR (EEG) OR (pschopatho\*) OR (bildgebend\* W/1 Verfahren) OR (magnetstimul\*) OR (elektrostimul\*) OR (cognitive) OR (Gehirndop\*) OR (Alzheimer\*) OR (Parkinson\*) OR (Multiple W/1 Sklerose\*) OR (ADHS) OR (physiolog\*) OR (stoffwechsel\*) OR (gedächtnis\*) OR (gedaechtnis\*) OR (protein\*) OR (elektrophysio\*) OR (zyto\*) OR (zell\*) OR (Reaktionszeit) OR (Zentralnerven\*) OR (erinnerung\*) OR (bewusstsein\*) OR (Computerchip\*) OR (implant\*) OR (wahrnehmung\*) OR (elektrische\* W/2 stimul\*))) 

**Antibiotikaresistenz**    
antibio\* AND resist\*


### IV Das Mediensample

Erfasst wird die Berichterstattung in folgenden Medientiteln:

**Nachrichtenmagazine**: Der Spiegel, Der Stern    
**Überregionale Tageszeitungen**: Die Welt, taz     
**Regionalzeitungen**: Berliner Zeitung, Nürnberger Nachrichten

Abgerufen werden die Beiträge unter Verwendung des jeweiligen Suchstrings aus der Datenbank *Nexis*: [nexisuni.com](https://www.nexisuni.com)


### V Die Untersuchungszeiträume

Für die zeitvergleichende Analyse werden zwei Untersuchungszeiträume erhoben. In Abhängigkeit von der Trefferzahl der identifizierten Beiträge unterscheiden sich die Untersuchungszeiträume bei den Themen.

**Biotechnologie**    
1.1.2000 - 31.12.2001    
1.1.2016 – 31.12.2019    

**Klimawandel**    
1.1.2000 - 31.12.2001    
1.1.2018 – 31.12.2019    

**Neurowissenschaften**    
1.1.2000 - 31.12.2001    
1.1.2017 – 31.12.2019    

**Antibiotikaresistenz**    
Ausschließlich bei diesem Thema werden Artikel aus jedem einzelnen Untersuchungsjahr seit dem 1.1.2000 bis einschließlich 31.12.2019 ausgewählt.

### VI Methode

Für die Codierung werden sowohl computergestützte, automatisierte Verfahren als auch Mittel der klassischen (manuellen) Inhaltsanalyse eingesetzt.    
Unser Ziel besteht in einem ersten Schritt darin, Akteur:innen, die sich an öffentlichen Debatten beteiligt haben, zu identifizieren. Hier kommt **Named entity recognition** (kurz: NER) als automatisiertes Verfahren zum Einsatz.    
In einem zweiten Schritt müssen wir die Akteur:innen, die das NER-Verfahren identifiziert hat, kategorisieren, d.h. wir müssen wissen, ob es sich um zentrale oder periphere Akteure im Sinne Habermas‘ (1992) handelt und darüber hinaus, aus welchem gesellschaftlichen Bereich (Politik, Wissenschaft usw.) die Akteur:innen stammen, welches Geschlecht und welche Nationalität die Akteur:innen aufweisen und ggf., welcher Institution/ Organisation und welchem wissenschaftlichen Forschungsbereich die (wissenschaftlichen) Akteur:innen angehören. Dazu bedienen wir uns der klassischen, manuellen Inhaltsanalyse. (Nur) Hier kommt dieses Codebuch zum Einsatz.
