### 1. Formale Kategorien - Beitragsebene

Die einzige Variable, die Sie hier selbst codieren müssen, ist die „Coder-ID“. Hier vergeben Sie die Zahl, die Ihnen als Codierer:in zugewiesen wurde.
Zusätzlich erhält jeder identifizierte Beitrag eine eigene ID. Auch seine Überschrift, der Medientitel, in dem der Beitrag erschienen ist sowie das Erscheinungsdatum des Beitrags werden erfasst. Diese Daten werden automatisiert erhoben. Eine manuelle Codierung ist nicht notwendig. Das gleiche gilt für die Kategoiren Ressort, Länge des Artikels und Autor:in des Artikels.

#### 1.1 Coder-ID (IDC)

Jede:r Codierer:in erhält eine spezifische Coder-ID. So können die Beiträge, die codiert wurden, einem:einer Codierer:in zugeordnet werden.

**20 Lehmkuhl, M./ Leidecker-Sandmann, M./ Promies, N.**

#### 1.2 Beitrags-ID (IDB)

Die Identifikationsnummer des Beitrags wird automatisiert erhoben. Eine manuelle Codierung ist nicht erforderlich.

#### 1.3 Überschrift des Beitrags (HEAD)

Die Überschrift des Beitrags wird automatisiert erhoben. Eine manuelle Codierung ist nicht erforderlich.

#### 1.4 Medientitel (MEDI)

Das Nachrichtenmedium, in dem der Beitrag erschienen ist, wird automatisiert erhoben. Eine manuelle Codierung ist nicht erforderlich.

#### 1.5 Datum (DATE)

Das Veröffentlichungsdatum des Beitrags wird automatisiert erhoben. Eine manuelle Codierung ist nicht erforderlich.

#### 1.6 Ressort (DESK)

Das Ressort, in dem der Beitrag veröffentlicht wurde, wird automatisiert erhoben. Eine manuelle Codierung ist nicht erforderlich.

#### 1.7 Länge (WORD)

Die Länge des Beitrags in Worten wird automatisiert erhoben. Eine manuelle Codierung ist nicht erforderlich.

#### 1.8 Autor:in (AUT)

Der Name bzw. das Kürzel des/der Autor:in des Beitrags wird automatisiert erhoben. Eine manuelle Codierung ist nicht erforderlich.