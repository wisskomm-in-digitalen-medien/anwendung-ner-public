### 2. Inhaltliche Kategorien – Akteursebene

Das oben bereits erwähnte NER-Verfahren identifiziert vier verschiedene Arten von Eigennamen innerhalb der ausgewählten Beiträge (Personen, Organisationen, Ortsnamen und Sonstiges). In einem Vorverarbeitungsschritt werden lediglich die individuellen Akteur:innen, also die Personen, für die weitere Analyse herausgefiltert. 

#### 2.1 Akteurs-ID (IDA)

Für jeden durch NER identifizierte(n) Akteur:in wird automatisiert eine Akteurs-ID generiert. Diese setzt sich zusammen aus der Nummer des Artikels (fortlaufend gezählt), der Nummer des Satzes innerhalb des Artikels, in dem der:die Akteur:in vorkommt und der Nummer des Akteurs innerhalb des Satzes (es können theoretisch mehrere Akteur:innen innerhalb eines Satzes erwähnt werden). Beispiel: #1300501 wäre der:die erste Akteur:in im fünften Satz des 13. Artikels.

#### 2.2 Name des Akteurs/ der Akteurin (NAME)

Der Name des:der Akteur:in wird automatisiert erfasst. Eine manuelle Codierung ist nicht notwendig.

#### 2.3 Filtervariable (FILT)

Diese Variable wird lediglich codiert, wenn 1) das NER-Verfahren eine(n) Akteur:in identifiziert hat, bei dem:der es sich nicht um eine Person handelt, 2) der:die Akteur:in innerhalb des Beitrags bereits einmal codiert wurde, 3) es sich bei dem:der ausgewiesenen Akteur:in um den:die Autor:in des Artikels handelt oder 4) der:die ausgewiesene Akteur:in lediglich passiv im Beitrag erwähnt wird und nicht selbst direkt oder indirekt im Artikel zu Wort kommt.    
In allen vier Fällen müssen die Variablen 2.4 – 2.8 nicht codiert werden!

**Vorgehensweise zu Punkt 4: Unterscheidung passive und aktive Akteur:innen:**    
Wir sind lediglich an den Akteur:innen interessiert, die in einem Artikel direkt (wörtliches Zitat, i.d.R. erkennbar an Anführungszeichen, bspw.: Meyer erklärt/ sagt: „Es verhält sich mit dem Virus soundso“) oder indirekt zu Wort kommen (Aussage des:der Akteur:in wird vom:von der:dem Autor:in des Artikels referiert, i.d.R. an Konjunktivkonstruktionen erkennbar wie bspw.: Laut Meyer sei es so, dass… ). Speziell für wissenschaftliche Akteur:innen gilt: Wenn von ihren Studienbefunden im Artikel berichtet wird, werten wir dies auch als indirekte Zitierung. Diejenigen Akteur:innen, die wörtlich oder indirekt im Beitrag zitiert werden, bezeichnen wir als aktive Akteur:innen.    
Diejenigen Akteur:innen hingegen, die im Beitrag nur (beiläufig) erwähnt werden, ohne dass sie sich äußern, bezeichnen wir als passive Akteur:innen. An ihnen sind wir nicht weiter interessiert. Beispiel: Der Gesundheitsminister Jens Spahn gerät zunehmend unter Druck./ Angela Merkel berät sich mit den Länderchefs. -> Solange diese Personen nur erwähnt werden und im Beitrag keine Aussagen tätigen, werden die nachfolgenden Kategorien 2.4 – 2.8 für sie nicht erhoben.    
Um eine:n Akteur:in als aktiv zu bezeichnen, reicht es aus, wenn diese:r ein einziges Mal im Beitrag wörtlich oder indirekt zitiert wird! Um zu prüfen, ob ein:e durch das NER-Verfahren identifizierte:r Akteur:in eine:n aktive:n oder passive:n Akteur:in darstellt, müssen Sie in den Fällen, in denen der:die Akteur:in bei der ersten Nennung nicht wörtlich oder indirekt zitiert wird, prüfen, ob er:sie im weiteren Verlauf des Beitrags noch einmal erwähnt wird und dann wörtlich oder indirekt zitiert wird. Hierzu können Sie sich in der Codier-App den gesamten Artikel anzeigen lassen (Button: „Kompletter Text“) – der (Nach-)Name des Akteurs/ der Akteurin wird Ihnen farblich markiert präsentiert.
Für den Fall, dass der:die Akteur:in im gesamten Beitrag nicht wörtlich oder indirekt zu Wort kommt, codieren Sie bitte „4 = Person ist ein:e passive:r Akteur:in“. Die Codierung dieses Akteurs/ dieser Akteurin ist damit abgeschlossen. Die App wird nach Ihrer Codierung automatisch zum:zur nächsten Akteur:in „springen“.    
Kommt der:die Akteurin an irgendeiner Stelle im Beitrag wörtlich oder indirekt zu Wort, codieren Sie bitte die Kategorien 2.4 – 2.8 für diese:n Akteur:in einmalig (egal, wie häufig er:sie zu Wort kommt). Die App wird nach Ihrer einmaligen Codierung automatisch zum:zur nächsten Akteur:in „springen“.    
**ACHTUNG**: Werden historische Persönlichkeiten zitiert/referiert oder enthält der Beitrag (indirekte) Zitate, die Personen in einer weiter zurückliegenden Vergangenheit getätigt haben (sozusagen „Reminiszenzen“), wie etwa – um Beispiele aus dem Codiermaterial zu nennen – Äußerungen von Robert Koch, Barbarossa oder dergleichen, dann werden diese Personen nicht als aktive, sondern als passive Akteur:innen gewertet! In der Vergangenheit getätigte Äußerungen werden nur dann als „aktive Aussagen“ codiert, wenn sie noch einen Gegenwartsbezug aufweisen.

**1 = Es handelt sich nicht um eine Person (auch fiktive Charaktere)**    
**2 = Akteur:in wurde in diesem Artikel bereits codiert**    
**3 = Person ist Autor:in des Artikels**    
**4 = Person ist ein:e passive:r Akteur:in**

#### 2.4 Institution/ Organisation (ORGA)

Einzutragen ist hier der Name der Institution oder Organisation, der der:die Akteur:in angehört (im Volltext erfassen). Dies kann beispielsweise der Name einer Hochschule, an der ein:eine Wissenschaftler:in forscht, sein oder eines Unternehmens. Die Institution wird nur codiert, sofern sie im Beitrag explizit erwähnt wird oder wenn sie aus der (Amts-)Bezeichnung des Akteurs/ der Akteurin hervorgeht. Wir beispielsweise von der Bundeskanzlerin gesprochen, können Sie als Institution „Bundeskanzleramt“ codieren. Wird der Gesundheitsminister erwähnt, können Sie „Gesundheitsministerium“ codieren. Die Institution wird so genau wie möglich codiert (mit Ortsangabe). Ansonsten (wenn die Institution nicht erkennbar ist) wird dieses Feld frei gelassen (etwa, wenn lediglich von „Forscher:in“ oder „Künstler:in“ gesprochen wird).

**ACHTUNG**: Bei der Codierung der Institution/ Organisation dürfen Sie insofern etwas „großzügiger“ sein, als Sie etwa bei bekannten Persönlichkeiten, wie Bundeskanzler:in, Bundesminister:innen, EU-Kommissar:innen, die Institutionszugehörigkeit – die allgemein bekannt sein dürfte – eintragen können, auch wenn sie nicht explizit genannt wurde, etwa: Bundeskanzleramt, Gesundheitsministerium, Europäische Kommission. Diese Information wird zwar nicht immer explizit im Beitrag erwähnt, aber der:die durchschnittliche Leser:in – wie wir so schön sagen und um dessen:deren Eindruck es uns geht –, weiß, dass Angela Merkel Bundeskanzlerin ist/ war.

#### 2.5 Geschlecht (SEX)

Hier wird das Geschlecht des:der Akteur:in erfasst. Auf das Geschlecht soll anhand des Namens des:der Akteur:in geschlossen werden bzw. aus der Anspache des:der Akteur:in (etwa Frau Müller oder Herr Mayer etc.). Ist die Zuordnung zu einem Geschlecht nicht eindeutig möglich, wird „99 = anderes/ nicht klar bestimmbar“ codiert.

**0 = männlich**    
**1 = weiblich**    
**99 = anderes/ nicht klar bestimmbar**
