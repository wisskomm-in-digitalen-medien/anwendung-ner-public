#### 2.8 Nationale Verortung (NAT)

Hier wird erfasst, ob es sich bei dem:der Akteur:in um eine Person, die (überwiegend) in Deutschland tätigt ist/ arbeitet handelt, oder um eine Person, die in einem anderen Land als Deutschland tätig ist.

ACHTUNG I: Es geht bei dieser Variable *nicht* um die Nationalität (Staatsbürgerschaft) der Person, sondern um ihren aktuellen Tätigkeitsort, bei Wissenschaftler:innen etwa, ob sie an einer deutschen Hochschule forschen, oder nicht. Oder handelt es sich um eine:n Politiker:in aus dem deutschen Bundestag, oder nicht.

Zur Codierung werden lediglich die Informationen innerhalb des Artikels herangezogen. Wenn aus den Artikelinformationen nicht klar hervorgeht, in welchem Land ein:e Akteur:in tätig ist, wird „99 = Nicht feststellbar“ codiert.

**1 = Deutschland**    
**2 = Anderes EU-Land**    
**3 = USA**    
**4 = Anderes Land**    
**5 = Supranational** (mehrere Länder, z.B., wenn ein:e Akteur:in Mitglied einer internationalen Organisation wie der WHO oder der EU (EU-Kommissar:in z.B.) oder einer internationalen Forscher:innengruppe ist)     
**99 = Nicht feststellbar**    

ACHTUNG II: Großbritannien wird als EU-Land codiert, da es im ersten Untersuchungszeitraum noch Mitgliedsstaat der EU war.
