#### 2.6 Zugehörigkeit zu Gesellschaftsbereich (AREA)

Diese Variable beschreibt einen gesellschaftlichen Bereich, welchem der:die Akteur:in zugeordnet werden kann. Bitte codieren Sie immer so spezifisch wie möglich (Unterkategorien, etwa „210“). Nur wenn keine Spezifizierung möglich ist, codieren Sie bitte die Oberkategorien (etwa „200“). Der Gesellschaftsbereich wird an der Institution, für die ein:e Akteur:in arbeitet, festgemacht, z.B. Sprecher:in des RKI = wissenschaftliche Administration.

**100 = Wissenschaft**    
Die Kategorie Wissenschaft umgreift Forscher:innen ohne politische oder soziale Funktionen.
„Wissenschaftler“, „Forscher“ oder „Biologe“ sind eindeutig wissenschaftliche Akteur:innen.    
Akteur:innen der DFG sind ebenso eindeutig wissenschaftlicher Akteur:innen.
Auch Mitglieder der IPCC zählen zu den Wissenschaftler:innen.
Ein Definitionskriterium für wissenschaftliche Akteur:innen ist, dass diese unabhängig/ objektiv arbeiten, also nicht im engeren Sinne interessengeleitet.

**ACHTUNG**: Wenn bei einem:einer Mediziner:in erkennbar wird, dass er:sie in seiner:ihrer Rolle als Wissenschaftler:in spricht (also forscht und nicht praktiziert), ist „100 = Wissenschaft“ zu codieren. Wenn es nicht klar erkennbar ist, dann wird „400 = Medizin“ codiert. Mitarbeiter:innen von Universitätskliniken (ausgenommen Pflegepersonal, Verwaltungspersonal), wie Chef- oder Oberärzte, werden als wissenschaftliche Akteur:innen mit der dazugehörigen wissenschaftlichen Disziplin „Medizin“ codiert.

**ACHTUNG II**: Auch Mitarbeiter:innen privater Forschungsinstitute werden den wissenschaftlichen Akteur:innen zugerechnet – NICHT aber Mitarbeiter:innen von wirtschaftlich orientierten Unternehmen, die auch Forschung betreiben (diese sind als Partialinteressenvertreter:innen zu codieren).

**ACHTUNG III**: Mitarbeiter:innen von Botanischen Gärten werden auch unter Wissenschaft codiert.

**200 = Politik**    
Zum politischen Bereich gehören explizit politische Akteur:innen wie Mitglieder von Regierungsinstitutionen, politischer Administration (z.B. Ministerien) sowie politischer Parteien. „CDU Mitglieder“ oder „EU- Abgeordnete“ sind politische Akteur:innen. 

**ACHTUNG**: Tauchen ehemalige Politiker:innen im Beitrag auf, codieren wir sie unter dem Akteurscode „200 – Politik“, also der Oberkategorie Politik.

Wir unterscheiden zwischen:

**210 = Politisch Exekutive**    
Politische exekutive Akteur:innen umfassen Regierungsmitglieder (EU- Kommission, Bund, Land oder Kommune). Sollte ein:e Akteur:in mehrere Rollen haben (Gröhe war sowohl Gesundheitsminister als auch Parteipolitiker (CDU)) ist die Rolle zu codieren, in der er:sie im jeweiligen Kontext spricht. Sollte das unklar sein, wird bei exekutiven Akteur:innen immer exekutiv codiert. Politisch exekutive Akteur:innen sind auch die UN-Mitglieder.

**220 = Politische Administration (Ministerien, Gesundheitsämter)**    
ACHTUNG: Wenn der:die Akteur:in Minister:in eines Ministeriums ist, ist er:sie als Regierungsvertreter:in mit „210“ zu codieren. Ab dem Level Staatssekretär:in wird ein:e Akteur:in mit „220“ codiert. Als politische Administration werden auch Akteur:innen codiert, die etwa der FAO (UN-Body), der UNESCO und dergleichen angehören.

**230 = Politische Legislative**    
Mitglieder der Parlamente auf allen möglichen Ebenen, also EU, Bund, Länder, Kreise und Gemeinden. Wir unterscheiden ggf. die Zugehörigkeit zu einzelnen Parteien:

  **231 = CDU**    
  **232 = SPD**    
  **233 = Grüne**    
  **234 = FDP**    
  **235 = AfD**    
  **236 = Linke**     
  **239 = Sonstige**

**300 = Wissenschaftliche Administration**    
Beschreibt die etwas engere Klasse von Mitgliedern wissenschaftlichen Institutionen, die auch administrative Funktionen ausführen. Dazu zählen die oben schon erwähnten Ressortforschungseinrichtungen, die einem Bundes- oder Landesministerium unterstellt sind, z.B. – um die Wichtigsten zu nennen – das Robert Koch Institut, das Bundesamt für Risikobewertung, das Friedrich Löffler Institut, das Paul Ehrlich Institut oder das Bundesamt für gesundheitlichen Verbraucherschutz. Bei Unsicherheit können Sie [diese Liste](https://www.ressortforschung.de/de/ueber_uns/mitglieder/index.htm) konsultieren (ACHTUNG: nichts zwangsweise vollständig!). Dazu gehören auch Mitglieder internationaler Institutionen wie der WHO oder der ECDC oder des amerikanischen CDC (Centers for Disease Control*) oder des NIH (National Instituts of Health). Beispiel: „WHO Experten“ ist als wiss. Administration zu codieren.

*Eine Liste der CDC Center/Institute/Offices finden Sie [hier](https://www.cdc.gov/publichealthgateway/federalprogramsandfunding/cdc-acronyms.html).

**400 = Medizin**     
Bezieht sich auf medizinische Fachleute, nämlich Ärzt:innen, nicht auf anderes Krankenhauspersonal allgemein (dies würde unter „900 = Sonstiger Bereich“ codiert werden).
**ACHTUNG**: Wenn eine Person in einem Artikel nur als Mediziner:in bezeichnet wird, codieren wir diese als medizinische:n Akteur:in (*nicht* wissenschaftliche:n Akteur:in).
 
**500 = Interessensverbände**     
Wir unterscheiden zwischen Interessenverbänden (Interessenverbände 1), die Kollektivgüter vertreten, wie etwa Umweltschutz, Tierschutz, Frieden, von solchen Interessenverbänden (Interessenverbände 2), die die Interessen bestimmter gesellschaftlicher Gruppen vertreten. Zu den Interessenverbänden 1 gehören etwa Greenpeace, Nabu, WWF, auch NGOs. Es wird hier nicht unterschieden, ob es sich um nationale oder internationale Akteur:innen handelt.

**510 = Interesseverbände 1 (Kollektivgüter)**    
Dazu zählen Akteur:innen, die so genannte Kollektivgüterinteressen vertreten, also etwa Umweltschutz etc.

**520 = Interesseverbände 2 (Partialinteressen)**    
Dazu zählen Mitglieder von Gewerkschaften, Kirchen, Vertreter:innen von Wirtschaftsunternehmen einschließlich der Pharmaunternehmen und dergleichen, ebenso von Patientenorganisationen (diese sind keine Kollektivgüterinteressenvertreter). Es wird nicht unterschieden, ob es sich um nationale oder internationale Akteur:innen handelt.

**ACHTUNG**: Auch Mitarbeiter:innen privater Unternehmen sind bei den Partialinteressenvertreter:innen zu verorten.

**900 = Sonstiger Bereich**    
Wenn der Bereich des:der Akteur:in nicht erkennbar ist (wenn z.B. einfach „Expert:innen“ zitiert werden), sind diese Akteur:innen als Sonstige zu codieren. Auch, wenn keiner der zuvor genannten Bereiche zutreffend erscheint, sondern andere im habermas’schen Sinne periphere Gesellschaftsbereiche angesprochen werden, wird „900“ codiert. Beispiele sind etwa „Museen“ oder „Zoos“.
