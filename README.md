## NER als Grundlage einer händischen Inhaltsanalyse

Named Entity Recognition (NER) ist die automatisierte Identifizierung von Eigennamen (etwa von Personen oder Organisationen) in Texten. Ohne weitere Verarbeitung sind die Ergebnisse von NER-Verfahren nur
für relativ wenige, eingeschränkte Fragestellungen von Interesse. Eine Möglichkeit der Nutzung von NER zur Untersuchung von komplexeren Fragestellungen ist die Kombination mit einer händischen Inhaltsanalyse.
In diesem Repository sind zwei Scripte enthalten, mit denen in einer Beispielanwendung NER als Vorverarbeitungsschritt in eine Akteursanalyse eingebunden wurde, um die händische Codierung deutlich zu beschleunigen und damit die Stärken 
automatisierter und händischer Codierung zu verbinden. Zentraler Bestandteil der Anwendung ist eine eigens entwickelte Codieroberfläche, die auf die Codierung der NER-Ergebnisse abgestimmt ist. Die Codierung findet vollständig
auf der Ebene einzelner Akteur:innen statt, die Codieroberfläche gibt zunächst immer nur den konkreten Satz aus, in dem ein Name erwähnt wird. In den meisten Fällen reichen die in diesem Satz enthaltenen Informationen
schon aus, um Eigenschaften der Person zu codieren, falls weitere Informationen benötigt werden, kann weiterer Kontext angezeigt werden. Dadurch, dass mit der Identifizierung der Personen ein relativ aufwendiger Schritt der 
Codierung entfällt, und die Codierung durch den Fokus auf kurze Textabschnitte sehr vereinfacht wird, ermöglicht der Ansatz die effiziente Codierung großer Textmengen.

Mit dem Script *create_actors_dataset.py* werden in Artikeln aus der LexisNexis-Datenbank Personennamen identifiziert (mit dem NER-Verfahren von [Flair](https://github.com/flairNLP/flair)) und eine CSV-Datei erzeugt, in der jede Zeile
einer Akteursnennung in einem Artikel entspricht. Diese Daten sind die Grundlage für die händische Codierung, bei der eine [Shiny](https://shiny.rstudio.com/)-App genutzt wird. Die App kann mit dem R-Script *app_akteurszugehoerigkeit.R* ausgeführt werden.
Anweisungen für die händische Codierung sind als Markdown-Dateien im Ordner *source_codebuch* hinterlegt, sie können während der Codierung in der App aufgerufen werden. 
